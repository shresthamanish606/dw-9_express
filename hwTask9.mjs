/* Make a function that takes input as number
and return output. You can watch movies if
input is greater or equal to 18 else return
"You are not authorized to watch movies."
*/

export let permission = (num)=>{
    if(num >= 18){
        return "You can watch movies."
    }
    else{
        return "You are not authorized to watch movies."
    }
}