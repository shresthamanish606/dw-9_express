let arrStr = JSON.stringify([1,2,3,4,5])  // '[1,2,3,4,5]'
let parseArrStr = JSON.parse(arrStr)      // [1,2,3,4,5]
console.log(arrStr)
console.log(typeof arrStr)
console.log(parseArrStr)
console.log(typeof parseArrStr)
