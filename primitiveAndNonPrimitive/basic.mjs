// In case of primitve data type, memory is created once variable is defined using let.
// But, in case of non-primitive data type, memory is created by checking if the variable that is going to be created is the copy of another variable or not. If that is the case then, the two variables share memory and changes in one variable results change in another too.\
// Above explanation is illustrated below with an example code.

// Primitive
// let a = 1
// let b = a
// a = 5
// console.log(a)
// console.log(b)

// Non-primitive
// let arr1 = [5,15]
// let arr2 = arr1
// arr1.pop()
// console.log(arr1)
// console.log(arr2)

// in primitive => for every let a new memory space is created
// in non-primitive => if there is let a new memory space is created
// but first it sees whether the variable is copy of another variable
// if the variable is copy of another variable then at that time, it will not create memory for that variable rather it shares memory


// let a = 1
// let b = a
// let c = 1
// console.log(a===b)  //In case of primitive, it looks that whether 'a' and 'b' contains same value or not.
// console.log(b===c)


// let arr1 = [5,6]
// let arr2 = arr1
// let arr3 = [8,9]
// console.log(arr1===arr2)  //In case of non-primitive, it looks whether both arrays have share memory address or not.
// console.log(arr1===arr3)


// let arr1 = [1,10]
// let arr2 = arr1
// console.log(arr1===arr2)
// console.log([1,10]===[1,10])