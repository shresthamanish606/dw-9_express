let obj = {
    name : "Harry",
    age : 23,
    inRelationship : false
}


let keyArr = Object.keys(obj)
console.log(keyArr)
let valueArr = Object.values(obj)
console.log(valueArr)
let propertiesArr = Object.entries(obj)
console.log(propertiesArr)