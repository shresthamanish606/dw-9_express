// Objects
// Object is more descriptive than array as it gives information of data
// Object is made by the combination of key and value pairs i.e. property
let obj = {
    name : "Mani",
    age : 23,
    isMarried : false
}
// Here, key = name, value = "Mani"

//get whole object 
console.log(obj)

// get specific element of object
console.log(obj.name)
console.log(obj.age)
console.log(obj.isMarried)

// change specific element of object
obj.name = "Mary"
console.log(obj)

// delete specific element of object
delete obj.age
console.log(obj)