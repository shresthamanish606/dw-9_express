// We cannot convert all array to objects
// We can only convert if we have array like[["name","David"],["age",23]]
// Here we have array of array and inner array has length of 2

let arr = [["name","David"], ["age",23]]

let info = Object.fromEntries(arr)
console.log(info)