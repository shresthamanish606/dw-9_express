// let {age, name, location} = {
//     name : "Manish",
//     location : "Dursanchar",
//     age : 23
// }
// in object, order doesn't matter
// console.log(name)
// console.log(location)
// console.log(age)


// Q. You have an object 'student' with properties 'name' and 'grades', where 'grades' is an array. Use object destructuring to extract the 'name' and the first element of the 'grades' array into separate variables.

// let student = {
//     name : "Manish",
//     grades : [55,67,71,80,59]
// }

// let {name, grades:[subGrade]} = student 
// console.log(name)
// console.log(subGrade)


// Q. Create a function that accepts an object with properties 'a','b', and'c'. Use object destructuring to assign default values of 0 to these properties if they are not present in the object.

// let funcObj = ({a=0,b=0,c=0})=>{
//     return `a : ${a}, b : ${b}, c : ${c}`
// }

// let output = funcObj({a:5})
// console.log(output)


// let logFruitNames = (...fruit)=>{
//     return fruit.forEach((value,i)=>{
//         console.log(value)
//     })
// }
// let fruits = ["mango","grapes","apple","orange","banana","pomegranate"]
// logFruitNames(...fruits)