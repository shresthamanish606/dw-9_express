// JS is synchronous,
//       single threaded,
//       blocking programming language.
// JS is case sensitive.
// +, -, *, / are operators.
// + operator act on both numbers and string.
// "manish" + "shrestha" = "manishshrestha"
// "manish" + " " + "shrestha" = "manish shrestha"
// " " -> blank also takes space in memory.
// 1 + 2 = 3
// "1" + "2" = "12"
// 1 + "1" = 11
// num + num = num
// "string" + "string" = "string"
// 1 + 2 + "3" + 1 + 1 = 3 + "3" + 1 + 1 = "33" + 1 +1 = "331" + 1 ="3311"
// If there is a war between string and number always string wins. 
// i.e. num + "string" = "string"
// operation are always performed between two values(operands). 


// 10/10/2023
// Comparator Operator
// ===, !==, <, >, <=, >=
// console.log(3===1);

// &&(AND ampersent) ||(OR pipe)
// && -> True -> if all true
// || -> True -> if one is true
// console.log(!true);
// console.log(!false);

// Rules to name variable
// Variables name must be descriptive and it must be in camelCase.
// Do not redefine or redeclare variable.

// In boolean all empty are falsy
// 0 -> false
// 1,2,3,....  -> false

// In string if there is any character then it is true
// "" -> false
// "a" -> true
// " " -> true
// "0" -> true

// if-else
// Higher priority block to be executed should always be at top

// October 11, 2023, Wednesday
// 
// We use backtick to call variables 
// String literal ` `
// The code after return does not execute, so write the return at the-
//      last of the function definition
// function without return -> call without storing in variable
// function with return -> call with variable


// October 12, 2023, Thursday
// Array can store multiple value of different data types.
// Array is made by the combination of value
// Object is made by the combination of key and value pairs i.e property
// Duplicate key does not exist in object
// Latest code override old codes 
// In object, order doesn't matter


// October 13, 2023, Friday


// October 31, 2023, Tuesday
// push,pop,unshift,shift => These methods change original array
// All method returns something but push,pop,unshift,shift change original array while reverse and sort method both returns and change original array
// Numbers sort doesn't work properly in JavaScript
// eg. let ar1 = [9,10]
// output => [10,9]
// nep.aasu@gmail.com  9860678385@@


// November 1, 2023, Wednesday
// If end index is missing, slice method will slice to the end
// map => array
// filter => array
// find => array element
// some => boolean
// every => boolean


// November 2, 2023, Thursday
// Practice questions

// November 3, 2023, Friday
// While object destructuring there might be conflict in between variables so, the solution to the problem is alias


// November 7, 2023 , Tuesday
// setTimeout and setInterval always execute at last
// setTimeout is asynchronous function
// anything that push task to background is called as asynchronous function
// Sir's POV
// setTimeout pass the function to the background(node) and attached timer to it
// the function at node will pass to the memory queue of node after timer has finished
// the function of memory queue will pass to the JS if all of the code of JS gets executed
// JS run the code
// Alt+Shift+A => Multi-line comments
// Math function to be explored