/*8.
Make a function that take a number
 if number >=3 console i am greater or equal to 3
 if number === 3 console i am 3
 if number < 3 console i am less than 3 
else show i am other*/

export let display = (num)=>{
    if(num >= 3){
        console.log("I am greater or equal to 3.")
    }
    else if(num === 3){
        console.log("I am 3.")
    }
    else if(num < 3){
        console.log("I am less than 3.")
    }
    else{
        console.log("I am other.")
    }
}
