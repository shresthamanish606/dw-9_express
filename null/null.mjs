let a;
console.log(a)
a = 5
console.log(a)
// a = undefined
// console.log(a)
// Never assign variable by undefined value
// Instead assign null value to the variable if you really want the variable to be null
a = null
console.log(a)

// null is used to empty the variable
