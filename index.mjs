import { sum } from './task1.mjs'
// import { avg } from './task2.mjs'
import { product } from './task3.mjs'
import { is18 } from './hwTask1.mjs'
import { isGreaterThan18 } from './hwTask2.mjs'
import {criteria} from './hwTask3.mjs'
import { avg } from './hwTask5.mjs'
import { category } from './hwTask6.mjs'
import { ticketPrice } from './hwTask7.mjs'
import { display } from './hwTask8.mjs'
import { permission } from './hwTask9.mjs'

// let _sum = sum(7,8)
// console.log(_sum)

// let _avg = avg(10,20,30)
// console.log(_avg)

// let __product = product(5,6)
// console.log(__product)

// Homework
// 1.
// let result = is18(18)
// console.log(result)

// 2.
// let result = isGreaterThan18(20)
// console.log(result)

// 3.
// let _criteria = criteria(19)
// console.log(_criteria)

// 4.
// let _isEven = isEven(10)
// console.log(_isEven)

// 5.
// let __avg = avg(5,1,15)
// console.log(__avg)

// 6.
// let _category = category(11)
// console.log(_category)

// 7.
// let _ticketPrice = ticketPrice(27)
// console.log(_ticketPrice)

// 8.
// display(8)

// 9.
let _permission = permission(23)
console.log(_permission)