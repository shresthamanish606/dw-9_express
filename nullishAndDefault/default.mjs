// if the value is truthy it takes the truthy value
// if the value is falsy it search(takes) the next value
// Here, || is used as default value
let a = ""
let b = a || 0 || null || "carry"
// let b = a || 0 || null  // if all are falsy it takes the last value
console.log(b)
// falsy value => "",0,undefined,null
