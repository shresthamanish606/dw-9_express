import { college, firstName, lastName } from "./second.mjs";

console.log("Hello world");
console.log(5);
console.log(true);
console.log(5+4);

let num1 = 5;
let num2 = 8;
let num3= 11;
let avg = (num1+num2+num3)/3;
console.log(avg);


// Day-3
// October 10, 2023, Tuesday
// Comparator Operator
console.log(5!==5);
console.log(!true);
console.log(!false);

let n1 = 5;
let n2 = 6;
let sum = n1 + n2;
console.log(sum);

// Conversion to number, string and boolean(truthy and falsy value)
console.log(Number('1'));
console.log(String(5));
console.log(String(true));
console.log(parseInt('3.5'));

// if-else
let isMarried = true;
if(isMarried){
    console.log("He is married");
}
else{
    console.log("He is not married");
}

// let age = 22;
// if(age > 18){
//     console.log("He can enter bar");
// }
// else{
//     console.log(("He cannot enter bar"));
// }

let gender = "male";
if(gender === "male"){
    console.log("He is male");
}
else if(gender == "female"){
    console.log("She is female");
}
else{
    console.log("They is other");
}

let customerAge = 20;
if(customerAge === 25){
    console.log("Your ticket is free");
}
else if(customerAge === 26){
    console.log("Your ticket is 100");
}
else if(customerAge === 27){
    console.log("Your ticket is 200");
}
else{
    console.log("You are not allowed");
}


let age = 23;
if(age>=1 && age<=17){
    console.log("Your ticket is free");
}
else if(age>=18 && age<=25){
    console.log("Your ticket cost 100");
}
else{
    console.log("Your ticket cost 200");
}


// Day-4
// October 11, 2023, Wednesday
// Check if the given number is even or not
let num = 8;
if(num%2 === 0){
    console.log("Given number is even");
}
else{
    console.log("Given number is odd");
}

let name = "Manish";
let surname = "Shrestha";
let fullName = `${name} ${surname}`;
// console.log(`${fullName}`);
console.log(fullName);

// import variable from another file
console.log(college);
console.log(`${firstName} ${lastName}`);

// undefined
let c;
console.log(c);
