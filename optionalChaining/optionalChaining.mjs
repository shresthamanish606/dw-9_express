let info = {
    name: "Harry",
    father : {
        age : 50,
    },
}
console.log(info?.father?.age)

//optional chaining =>  it gives output either undefined or data, it doesn't give error, rather it is used to eliminate error