// Sample array of employee objects
const employees = [
    { name: 'Alice', age: 28, department: 'HR', active: true },
    { name: 'Bob', age: 35, department: 'IT', active: false },
    { name: 'Charlie', age: 23, department: 'Marketing', active: true },
    { name: 'David', age: 40, department: 'Finance', active: true },
  ];
  
  // Your task is to:
  // 1. Filter out only the active employees.
  // 2. Map the remaining employees to create a new array of objects with only the name and department properties.
  // 3. Find an employee with a specific name.
  // 4. Check if there is any employee below a certain age.
  // 5. Determine if all employees belong to a specific department.
  
  // Write JavaScript code to achieve these tasks using the array and string methods you mentioned.
  

  let activeEmployees = employees.filter((value)=>{
    if(value.active === true){
        return true
    }
    else{
        return false
    }
  })
  console.log(activeEmployees)

  let inactiveEmployees = employees.filter((value)=>{
    if(value.active !== true){
        return true
    }
    else{
        return false
    }
})
console.log(inactiveEmployees)

let objToArr = Object.entries(...inactiveEmployees)
console.log(objToArr)

let filterObjToArr = objToArr.filter((value,i)=>{
    if(value[0] === "name" || value[0] === "department"){
        return true
    }
    else{
        return false
    }
})
console.log(filterObjToArr)

let arrToObj = Object.fromEntries(filterObjToArr)
console.log(arrToObj)


let specificEmployee = employees.find((value)=>{
    if(value.name === "Alice"){
        return true
    }
    else{
        return false
    }
})
console.log(specificEmployee)


let ageSpecification = employees.some((value)=>{
    if(value.age < 30){
        return true
    }
    else{
        return false
    }
})
console.log(ageSpecification)


let employeesSameDepartment = employees.every((value)=>{
    if(value.department === "IT"){
        return true
    }
    else{
        return false
    }
})
console.log(employeesSameDepartment)
