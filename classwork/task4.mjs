let products =[
    {name:"earphone", price:1000},
    {name:"battery",price:2000},
    {name:"charger",price:500}
    ]
// Find the sum of all product.

let productsTotalPrice = products.reduce((prev,curr)=>{
    return prev+curr.price
},0);
console.log(productsTotalPrice)


// output expected => [earphone, battery, charger]

let productsName = products.map((value,i)=>{
    return value.name
})
console.log(productsName)


// output expected => [1000,2000,500]
let productsPrice = products.map((value,i)=>{
    return value.price
})
console.log(productsPrice)


// output expected => price greater than 700 i.e. [1000,2000]

let productsFilteredPrice = products.filter((value,index)=>{
    if(value.price > 700){
        return true
    }
    else{
        return false
    }
})

let productsPriceOnly = productsFilteredPrice.map((value,i)=>{
    return value.price
})

let productsNameOnly = productsFilteredPrice.map((value,i)=>{
    return value.name
})

console.log(productsFilteredPrice)
console.log(productsPriceOnly)
console.log(productsNameOnly)