// While object destructuring there might be conflict in between variables so, the solution to the problem is alias

let age = 23

let {name, age:age1} = {name : "Ram", age : 30}
console.log(age)
console.log(name)
console.log(age1)

// let {name:name1, age:age1} = {name : "Ram", age : 30}
// console.log(age)
// console.log(name1)
// console.log(age1)