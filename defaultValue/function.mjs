let show = (a,b=8)=>{
    // if b is not passed as argument then it will take default value as 8
    console.log(a)
    console.log(b)
}

show(1,10)