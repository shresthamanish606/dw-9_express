// If end index is missing, slice method will slice to the end
let words = ["magic", "created", "spun", "history"]
let _words = words.slice(1,3) // 1 is the starting index and 3 is end index(end index must be always greater than 1)
console.log(_words)


