let inputArr = ['a','b','c','d','e']

// let outputArr = inputArr.splice(1,3,'ram','hari','shyam',true)
// inputArr.splice(1,0,'ram','hari','shyam',true)
// 1 is starting index
// 3 no. of elements to delete
// ["ram", "hari", "shyam", true] are the elements to be added from starting index

// console.log(inputArr)
// console.log(outputArr) // It returns the deleted item


let outputArr1 = inputArr.splice(1) // if no. of item to be deleted is missing then it will remove all till the end
console.log(inputArr)
console.log(outputArr1)