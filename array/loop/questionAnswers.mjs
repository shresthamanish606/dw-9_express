// Q1. [1,2,3] => [11,12,13]
// let num = [1,2,3]

// let result = num.map((value, index)=>{
//     return value + 10
// })
// console.log(result)


// Q2. ["my", "name", "is"] = ["MY", "NAME", "IS"]
let words = ["my", "name", "is"]

let result = words.map((value, index)=>{
    return value.toUpperCase()
})
console.log(result)