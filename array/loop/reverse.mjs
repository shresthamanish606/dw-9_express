// All method returns something but push,pop,unshift,shift change original array while reverse and sort method both returns and change original array

// let alphabets = ['a','b','c','d']
// let _alphabets = alphabets.reverse()
// console.log(alphabets)
// console.log(_alphabets)


// let word = 'abc'
// let arrWord = word.split("")
// let reverseArrWord = arrWord.reverse()
// let strArrWord = arrWord.join("")
// console.log(strArrWord)
//      OR

// let reverseWord = word.split("").reverse().join("")
// console.log(reverseWord)

let sentence = "My name is"

let reverseSentence = sentence.split(" ").reverse().join(" ")
console.log(reverseSentence)