// let myDetails = ["ram",25,true]

// let _myDetails = myDetails.map((value, index)=>{
//     return value
// })
// console.log(_myDetails)
// input array => output array
// input length => output length

// let numbers = [1,2,3,4,5]

// let _numbers = numbers.map((value, index)=>{
//     return value * index
// })
// console.log(_numbers)


let arr = [1,2,3,4,5,6,7,8,9,10]

let result = arr.map((value)=>{
    return `2 * ${value} =  ${2*value}`
})
console.log(result)

// let arr = [1,2,3]

// let _arr = arr.map((value, index)=>{
//     return value*2
// })

// console.log(_arr)