// 1. Given an array of numbers, use 'forEach' to print each number
// to the console.

// let numbers = [1,2,3,4,5]

// numbers.forEach((value, i)=>{
//     console.log(value)
// })


// 2. Given an array of strings, use 'forEach' to 
// create a new array with the lengths of each 
// string.

// let collections = ["photos", "message", "specific", "conversation"]

// collections.forEach(
//     let collectionsLength = collections.map((value, i)=>{
//         return value.length
//     })
// )

// console.log(collectionsLength)


// 3. Given an array of numbers, use 'map' to square each 
// number and return a new array with the squared values.

// let nums = [1,2,5,9,11]

// let squaredNums = nums.map((value, i)=>{
//     return value * value
// })

// console.log(squaredNums)


// 4. Given an array of names, use 'map' to create a new array
// with the names in uppercase.

// let names = ["Saman", "Ayushma", "Nerisha", "Samira"]

// let namesUppercase = names.map((value, i)=>{
//     return value.toUpperCase()
// })

// console.log(namesUppercase)


// 5. Give an array of numbers, use 'filter' to create a new 
// array containing only the even numbers.

// let randomNumbers = [5,4,10,14,11]

// let evenNumbers = randomNumbers.filter((value,i)=>{
//     if(value%2 === 0){
//         return true
//     }
//     else{
//         return false
//     }
// })

// console.log(evenNumbers)


// 6. Given an array of objects representing books with 'title'
// properties, use 'find' to find the first book with a specific
// title

let books = [
    {title : "Micky and Mouse"},
    {title : "Mother the Lover"},
    {title : "Housekeeper wife"},
]

let specificTitle = "Mother the Lover"

let resultingBooks = books.find((value, i)=>{
    if(value.title === specificTitle){
        console.log(value.title)
    }
    else{
        return false
    }
})

console.log(resultingBooks)