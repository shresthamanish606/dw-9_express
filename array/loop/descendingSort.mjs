// Numbers sort doesn't work properly in JavaScript
// eg. let ar1 = [9,10]
// output => [10,9]
// let ar1 = [9,10]
// let _ar1 = ar1.sort()
// console.log(ar1)



let fruits = ["grapes", "apple", "banana", "mango", "pomegranate"]
let _fruits = fruits.sort().reverse()
console.log(_fruits)

