// filter only truthy value

let arr5 = [1, "harry", false, 0, undefined,"", "#","@123"]

// let _arr5 = arr5.filter(Boolean)
// console.log(_arr5)

        // OR

// let _arr5 = arr5.filter((value, i)=>{
//     if(value){
//         return true
//     }
//     else{
//         return false
//     }
// })

// console.log(_arr5)
