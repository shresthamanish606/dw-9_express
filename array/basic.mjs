let arr1 = ['james',23,true]

// Array can store multiple value of different data types.

// get whole array
console.log(arr1)

// get specific elements of array
console.log(arr1[0])
console.log(arr1[1])
console.log(arr1[2])

// change specific element of array
arr1[0] = "harry"
arr1[2] = false

console.log(arr1[0])
console.log(arr1[1])
console.log(arr1[2])

// delete specific element of array
delete arr1[1]
console.log(arr1)