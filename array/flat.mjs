// It is used to open wrapper of array
let arr1 = [
    1,
    2,
    [3,4],
    [
        [5,6],
        [7,8]
    ]
]

let arr2 = arr1.flat(2)
console.log(arr2)