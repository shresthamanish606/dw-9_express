let arr = [1,2,3]
let type = typeof arr
console.log(type)
console.log(typeof {name : "Harry"})
console.log(typeof new Set([1,2,3,3]))
console.log(typeof new Date())
console.log(typeof new Error("Unexpected error occur"))

// It means type of non-primitive is object.