// make a arrow function that takes a input and return its lowercase version

let makeLowerCase = ((word)=>{
    return word.toLowerCase()
})

let _makeLowerCase = makeLowerCase("Hello")
console.log(_makeLowerCase)