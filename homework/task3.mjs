// make a arrow function that takes a input and return another string which is a trim version (remove both start and end space)

let makeTrim = (sentence)=>{
    return sentence.trim()
}

let _makeTrim = makeTrim(" Hello Saturday ")
console.log(_makeTrim)